#include "FreeRTOS.h"
#include "task.h"

#include "gpio.h"

gpio_s my_led;
gpio_s my_switch;
void switch_led_logic__initialize(void);
void switch_led_logic__run_once(void);
